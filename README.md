sympa-design
============

Ideas, mockups and wireframes for the [Sympa](https://www.sympa.org/) mailing list server.

Mathieu Jourdan, 2016.
Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the [Free Art License](http://artlibre.org/licence/lal/en/)

