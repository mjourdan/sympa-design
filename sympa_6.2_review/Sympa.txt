List home
=========

Issues :

- sidebar shows 3 kind of objects : information (owners, moderators), links to other views (archive...) and actions (contact owners, subscribe...)
- inactive links are shown, without a clue if it is possible to enable them
- link to the list home is drawn in the sidebar
- the page lacks some info, we can't even know if it is about a discussion list or a newsletter

Proposal:

- help user to decide to subscribe or not, by showing her the type of the list, audience and trafic
- clarify interface by separating different the kind of items in the sidebar

List Creation - quick access
============================

Use case
--------

A person just wants to create a list. She got told to go to the sympa url, but she has never used Sympa. She doesn't plan to use it again, once the list is created

Issue
-----

From the welcome screen, it is unclear where user should go to create a list. There is a hint that some features require authentication, but user can't know for sure if she is really supposed to authenticate, which means enter the process to create an account.

Proposal
--------

Add a link on the home page to create a list, even if the user is not authentified.


List Creation - simple process
==============================

Issue
-----

When creating a list, user has to select one amongst eight list types (or templates). Templates names are sometimes not really meaningful ("Mailing list for intranets"), sometimes a bit verbose ("Mailing list configuration to be used for a news letter providing both text plain and HTML formats"). Template descriptions are note always helpful: who can tell the difference between "public archives / only subscribers can post" and "Messages can either be read via email (subscription) or via the web (web archives)"? Actually, each type corresponds to a specific set of settings: who can see the list exists, who can subscribe, who can post, who can read archives, are messages moderated...

So, what do a user do to create a list? She reads the eight templates names and descriptions, tries to identify the possible settings, set (mentally) her own values, guess which template matches the closest her settings, and finally pick up one without knowing for sure it actually matches what she was looking for.

Proposal
--------

### Information

List name: [    ]
Subject: [    ]
Topic: [   ] [+]
Description: [    ]

### Settings

visibility :	public (anyone can see the list)
		private	(only subscribers can see the list)
		secret (only owners can see the list)

posting: 	anybody (anyone can send messages to the list)
		subscribers (only subscribers can send messages)
		owners (only owners can send messages)
[ ] moderated (messages must be approved before beeing published) - except when only owners can send

subscriptions:	open (anyone can subscribe)
		controled (subscriptions require owners' approval)
		none (only subscribers can add members to the list)

archives:	public (anyone can browse the archives)
		private	(only subscribers can browse the archives)
		secret (only owners can browse the archives)
		
default reception format: text / html


